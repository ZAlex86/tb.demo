﻿using System;

namespace Demo_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 16;
            if (age > 18)
            {
                Console.WriteLine("vous êtes majeur");
            }
            else if(age <18)
            {
                Console.WriteLine("vous êtes mineur");
            }
            else
            {
                Console.WriteLine("ne sais pas");
            }
            // opérateur ternaire

            Console.WriteLine(age >= 18 ? "vous etes majeur " :(age <18 ? "vous etes mineur" : "ne sais pas"));
            Console.ReadKey();

            Console.WriteLine(value);

            int prix = 20;
            int? promotion = null;

            //coalesce
            Console.WriteLine(prix - (prix * (promotion ?? 0) / 100));
            Console.WriteLine(prix - (prix * (promotion != null ? promotion : 0) / 100));

            // null safe operator
            string maChaine = null;

            Console.WriteLine(maChaine?.ToLower());



            Console.ReadKey();
        }
    }
}
