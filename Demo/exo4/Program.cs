﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace exo4
{
    class Program
    {
        static void Main(string[] args)
        {
            #region ma solu
            //créer un dictionnaire vide // bar <nom,qt>
            //demander à l'utilistateur de pouvoir ajouter une clé dans ce dictionnaire
            //demander à l'utilisateur de spécifier la quantité
            //afficher l'état du stock

            //Dictionary<string, int> bar = new Dictionary<string, int>();
            //Console.WriteLine("ajouter une bière au dictionnaire");
            //string biere = Console.ReadLine();
            //Console.WriteLine("ajouter la quantité de cette bière au dictionnaire");
            //int.TryParse(Console.ReadLine(), out int nbl);
            ////Jupiler,orval

            //if (!bar.ContainsKey(biere))
            //{
            //    bar.Add(biere, nbl);
            //}

            //foreach (KeyValuePair<string, int> kvp in bar)
            //{
            //    Console.WriteLine($"{kvp.Key} : {kvp.Value}" + "L");
            //}
            //Console.ReadKey(); 

            #endregion

            // Bonus: Sauvegarde dans un fichier 
            // Créer un dictionnaire vide //bar <nom, qt>
            Dictionary<string, int> bar = new Dictionary<string, int>();
            // dans une boucle
            while (true)
            {
                Console.WriteLine("1.Ajouter");
                Console.WriteLine("2.Modifier");
                Console.WriteLine("3.Supprimer");
                //Console.WriteLine("4.Sauvegarder");
                int choix = int.Parse(Console.ReadLine());

                if (choix == 1)
                {
                    // demander à l'utilisateur le nom de la clé à ajouter dans ce dictionnaire
                    Console.WriteLine("Quel Produit souhaitez-vous ajouter ?");
                    // demander à l'utilisateur de spcécifier la quantité
                    string nom = Console.ReadLine();
                    // changer les lettres en maj
                    nom = nom.ToUpper();
                    // changer les lettres en min
                    // nom = nom.ToLower();
                    while (bar.ContainsKey(nom))
                    {
                        Console.WriteLine("Le produit existe déjà ?");
                        // demander à l'utilisateur de spcécifier la quantité
                        nom = Console.ReadLine();
                    }
                    Console.WriteLine("Quelle quantité ?");
                    string val = Console.ReadLine();
                    int nb;
                    while (!int.TryParse(val, out nb))
                    {
                        Console.WriteLine("Quantité non valide");
                        val = Console.ReadLine();
                    }
                    // Ajouter les infos dans le dico
                    bar.Add(nom, nb);
                }
                else if (choix == 2)
                {
                    // demander à l'utilsateur le produit qu'il souhaite modifier
                    // demander la quantité
                    // modifier dans le dico
                    Console.WriteLine("Quel Produit souhaitez-vous modifier ?");
                    string nom = Console.ReadLine();
                    nom = nom.ToUpper();
                    bool choix2 = true;
                    while (bar.ContainsKey(nom) && choix2 == true)
                    {
                        Console.WriteLine("Quelle quantité ?");
                        int.TryParse(Console.ReadLine(), out int nb);

                        bar[nom] = nb;
                        Console.WriteLine("Le produit " + nom + " a été modifier");
                        choix2 = false;
                    }

                    Console.WriteLine("voullez vous modifier un autre produit ?");
                    string entree = Console.ReadLine();
                    
                    
                }
                else if (choix == 3)
                {
                    //demander le produit à supprimer
                    //supprimer du dico
                    Console.WriteLine("Quel Produit souhaitez-vous supprimer ?");
                    string nom = Console.ReadLine();
                    nom = nom.ToUpper();
                    bool choix2 = true;
                    while (bar.ContainsKey(nom)&& choix2 == true)
                    {
                        bar.Remove(nom);
                        Console.WriteLine("Le produit "+nom+" a été supprimer");
                        choix2 = false;
                        
                    }

                }
                //else if (choix ==4)
                //{
                //    string ListDuBar = "bar.txt";
                //    if (!File.Exists(ListDuBar))
                //    {
                //        FileStream fs = File.Create(ListDuBar);
                //        fs.Close();
                //    }

                //}
                // afficher l'état du stock
                Console.Clear();
                foreach (KeyValuePair<string, int> kvp in bar)
                {
                    Console.WriteLine($"{kvp.Key}: {kvp.Value}");
                }
            }
        }
    }
}
