﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo5
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.Fonctions:
            //1.1 Créer une fonction qui prend 2 paramètres(nom et age) et qui affichera "Bonjour je m'appelle ... et j'ai ... ans".
            string nom = "alex";
            int age = 26;
            message(nom, age);
            Console.ReadKey();

            //1.2 Créer une fonction CalculeMoyenne(List<int> list) qui prend un paramètre de type List<int> et qui en retourne la moyenne.

            List<int> liste = new List<int>();
            liste.Add(44);
            liste.Add(25);
            CalculeMoyenne(liste);


            //1.3 Créer une fonction qui prend en paramètre une liste<int>
            //     et qui retourne une liste dans laquelle chacun des éléments sont multipliés par 2
            //     (la liste originale ne doit pas être modifiée).

            List<int> liste1 = new List<int>();
            afficherlaliste2(liste1);

            //1.4 Créer une fonction qui prend en paramètre une liste<int>
            //     et qui ne retourne rien dans laquelle tous les éléments sont élevés au carré.

            List<int> liste3 = new List<int>();
            listcarre(liste3);

            //1.5 Créer une fonction qui prend en paramètre un nombre n et qui retourne
            //    le nième nombre de la suite de Fibonacci(2 solutions bonus: while ou recurvité)
            //    F0 = 1
            //    F1 = 1
            //    F2 = F0 + F1 = 2
            //    F3 = F1 + F2 = 3
            //    F4 = 5

            //1.6 Bonus: Créer une fonction qui prend en paramètre un double et qui retourne sa racine carrée


        }

        private static void listcarre(List<int> liste3)
        {
            for (int i = 0; i < liste3.Count; i++)
            {
                liste3[i] = (liste3[i] * liste3[i]);
            }
            for (int i = 0; i < liste3.Count; i++)
            {
                Console.WriteLine(liste3[i]);
            }
        }

        private static void afficherlaliste2(List<int> liste1)
        {
            List<int> liste2 = new List<int>();
            liste1.Add(1);
            liste1.Add(2);


            for (int i = 0; i < liste1.Count; i++)
            {
                liste2.Add(liste1[i] * 2);
            }
            for (int i = 0; i < liste2.Count; i++)
            {
                Console.WriteLine(liste2[i]);
            }
        }

        private static double CalculeMoyenne(List<int> liste)
        {
            int total = 0;
            int grandeur = 0;
            foreach (int item in liste)
            {
                total += item;
                grandeur += 1;
            }
            double mo = total / grandeur;

            return mo;          
        }

        private static void message(string nom, int age)
        {
            Console.WriteLine("Bonjour je m'appelle " + nom + " et j'ai " + age + " ans");
        }
    }
}
