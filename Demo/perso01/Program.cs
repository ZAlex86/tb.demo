﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perso01
{
    class Program
    {
        static void Main(string[] args)
        {

            //initialisation d'un tableau et remplissage des éspaces.
            int[,] tabgrille = new int[13, 13];
            int joueur1 = 1;
            int joueur2 = 2;
            int tourjoueur;
            int compteur = 42;

            

            //interface graphique
            InterfaceGraph(tabgrille);
            
            bool ganger = false;
            int vainqueur = 0;
            //le jeu
            while (!ganger)
            {
                int colonne=0;
                if (compteur % 2 == 0)
                {
                    tourjoueur = joueur1;

                }
                else
                {
                    tourjoueur = joueur2;
                }
                Console.WriteLine("C\'est le tour du joueur " + tourjoueur);
                Console.WriteLine("Veuillez entrer la colonne entre 1 et 7");
                int.TryParse(Console.ReadLine(), out int entree);
                if (entree == 1)
                    colonne = 3;
                else if(entree == 2)
                    colonne = 4;
                else if (entree == 3)
                    colonne = 5;
                else if (entree == 4)
                    colonne = 6;
                else if (entree == 5)
                    colonne = 7;
                else if (entree == 6)
                    colonne = 8;
                else if (entree == 7)
                    colonne = 9;
                

                if (tabgrille[3, colonne] == 0)
                {
                    tabgrille[3, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 3))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 3))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkdiago(tabgrille, colonne, 3))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    
                   compteur -= 1;
                }
                else if (tabgrille[4, colonne] == 0)
                {
                    tabgrille[4, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 4))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 4))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkdiago(tabgrille, colonne, 4))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }

                    compteur -= 1;
                }
                else if (tabgrille[5, colonne] == 0)
                {
                    tabgrille[5, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 5))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 5))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkdiago(tabgrille, colonne, 5))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }

                    compteur -= 1;
                }
                else if (tabgrille[6, colonne] == 0)
                {
                    tabgrille[6, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 6))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 6))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkdiago(tabgrille, colonne, 6))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }

                    compteur -= 1;
                }
                else if (tabgrille[7, colonne] == 0)
                {
                    tabgrille[7, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 7))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 7))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkdiago(tabgrille, colonne, 7))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }


                    compteur -= 1;
                }
                else if (tabgrille[8, colonne] == 0)
                {
                    tabgrille[8, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 8))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 8))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkdiago(tabgrille, colonne, 8))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }

                    compteur -= 1;
                }
                else if (tabgrille[9, colonne] == 0)
                {
                    tabgrille[9, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 9))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 9))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkdiago(tabgrille, colonne, 9))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }

                    compteur -= 1;
                }
                else
                {
                    Console.WriteLine("cette colonne est  déjà remplie");
                    Console.ReadKey();
                }


                Console.Clear();
                InterfaceGraph(tabgrille);

            }
            Console.WriteLine("le joueur " + vainqueur + " a gagné!!!");
            Console.ReadKey();



        }
        public static bool checkverti(int[,] tabgrille, int colonne, int hauteur)
        {
            if (hauteur < 3)
                return false;
            else if (tabgrille[hauteur, colonne] != tabgrille[hauteur - 1, colonne] || tabgrille[hauteur, colonne] != tabgrille[hauteur - 2, colonne] || tabgrille[hauteur, colonne] != tabgrille[hauteur - 3, colonne])
                return false;
            else
                return true;
        }

        public static bool checkhori(int[,] tabgrille, int colonne, int hauteur)
        {
            if ((colonne == 3) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 3]))
                return true;
            else if (((colonne == 4) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2])) || ((colonne == 4) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 3])))
                return true;

            else if ((((colonne == 5) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1])) || ((colonne == 5) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2])) || ((colonne == 5) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 3]))))
                return true;

            else if (((colonne == 6) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 3])) || ((colonne == 6) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1])) || ((colonne == 6) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2])) || ((colonne == 6) && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 3]))
                return true;
            else if (((colonne == 7) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2])) || ((colonne == 7) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1])) || ((colonne == 7) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 3])))

                return true;
            else if (((colonne == 8) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 3])) || ((colonne == 8) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2])))
                return true;
            else if ((colonne == 9) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 3]))
                return true;
            else
                return false;
        }

        public static bool checkdiago(int[,] tabgrille, int colonne, int hauteur)
        {
            if((tabgrille[hauteur, colonne] == tabgrille[hauteur-1, colonne - 1]&& tabgrille[hauteur, colonne] == tabgrille[hauteur-2, colonne - 2]&& tabgrille[hauteur, colonne] == tabgrille[hauteur-3, colonne - 3])||(tabgrille[hauteur, colonne] == tabgrille[hauteur-1, colonne - 1]&& tabgrille[hauteur, colonne] == tabgrille[hauteur-2, colonne - 2]&& tabgrille[hauteur, colonne] == tabgrille[hauteur+1, colonne + 1])||(tabgrille[hauteur, colonne] == tabgrille[hauteur-1, colonne - 1]&& tabgrille[hauteur, colonne] == tabgrille[hauteur+1, colonne + 1]&& tabgrille[hauteur, colonne] == tabgrille[hauteur+2, colonne +2])||(tabgrille[hauteur, colonne] == tabgrille[hauteur+1, colonne + 1]&& tabgrille[hauteur, colonne] == tabgrille[hauteur+2, colonne +2]&& tabgrille[hauteur, colonne] == tabgrille[hauteur+3, colonne +3]))

                    return true;
            else if((tabgrille[hauteur, colonne] == tabgrille[hauteur+1, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur + 2, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur + 3, colonne - 3])|| (tabgrille[hauteur, colonne] == tabgrille[hauteur + 1, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur + 2, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur - 1, colonne + 1]) || (tabgrille[hauteur, colonne] == tabgrille[hauteur + 1, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur - 1, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur - 2, colonne + 2]) || (tabgrille[hauteur, colonne] == tabgrille[hauteur - 1, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur - 2, colonne + 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur - 3, colonne + 3]))
                return true;
            else
                return false;
        }



        public static void InterfaceGraph(int[,] tabgrille)
        {
            Console.WriteLine("          Puissance 4         ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[9, 3] + " | " + tabgrille[9, 4] + " | " + tabgrille[9, 5] + " | " + tabgrille[9, 6] + " | " + tabgrille[9, 7] + " | " + tabgrille[9, 8] + " | " + tabgrille[9, 9] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[8, 3] + " | " + tabgrille[8, 4] + " | " + tabgrille[8, 5] + " | " + tabgrille[8, 6] + " | " + tabgrille[8, 7] + " | " + tabgrille[8, 8] + " | " + tabgrille[8, 9] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[7, 3] + " | " + tabgrille[7, 4] + " | " + tabgrille[7, 5] + " | " + tabgrille[7, 6] + " | " + tabgrille[7, 7] + " | " + tabgrille[7, 8] + " | " + tabgrille[7, 9] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[6, 3] + " | " + tabgrille[6, 4] + " | " + tabgrille[6, 5] + " | " + tabgrille[6, 6] + " | " + tabgrille[6, 7] + " | " + tabgrille[6, 8] + " | " + tabgrille[6, 9] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[5, 3] + " | " + tabgrille[5, 4] + " | " + tabgrille[5, 5] + " | " + tabgrille[5, 6] + " | " + tabgrille[5, 7] + " | " + tabgrille[5, 8] + " | " + tabgrille[5, 9] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[4, 3] + " | " + tabgrille[4, 4] + " | " + tabgrille[4, 5] + " | " + tabgrille[4, 6] + " | " + tabgrille[4, 7] + " | " + tabgrille[4, 8] + " | " + tabgrille[4, 9] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[3, 3] + " | " + tabgrille[3, 4] + " | " + tabgrille[3, 5] + " | " + tabgrille[3, 6] + " | " + tabgrille[3, 7] + " | " + tabgrille[3, 8] + " | " + tabgrille[3, 9] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | 1 | 2 | 3 | 4 | 5 | 6 | 7 |");
        }
    }
}
