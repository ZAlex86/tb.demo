﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo8
{
    class Program
    {
        static void Main(string[] args)
        {
            // tableau
            string[] tableau = new string[5]
            {
                "a", "b", "c", "d", "e"
            };
            //indexé
            Console.WriteLine(tableau[4]);
            // Dictionnaires
            // Dictionary<typeDeLaCle,TypeDeLaValeur>
            Dictionary<string, int> mois = new Dictionary<string, int>();
            mois.Add("Jan", 31);
            //autre syntaxe
            //mois["Jan"] = 31;
            mois.Add("Feb", 28);
            mois.Add("Mar", 31);
            mois.Add("Apr", 30);
            mois.Add("Mai", 31);
            mois.Add("Jun", 30);
            mois.Add("Jul", 31);
            mois.Add("Agu", 31);
            mois.Add("Sep", 30);
            mois.Add("Oct", 31);
            mois.Add("Nov", 30);
            mois.Add("Dec", 31);

            // vérifier que la clé n'existe pas avant l'ajout
            if (!mois.ContainsKey("Dec"))
            {
                mois.Add("Dec", 31);
            }
            Console.WriteLine("Ecrivez le nom d'un mois");
            string m = Console.ReadLine();
            //Console.WriteLine($"Le mois demandé compte {mois[m]} jours");

            bool reussi = mois.TryGetValue(m, out int nbJours);
            if (reussi)
            {
                Console.WriteLine($"Le mois demandé compte {nbJours} jours");
            }
            else
            {
                Console.WriteLine("Le mois demandé n'existe pas");
            }

            //retirer une ligne du dictionnaire 
            //mois.Remove("Oct");

            //Console.WriteLine(mois["Oct"]);

            foreach (KeyValuePair<string, int> kvp in mois)
            {
                Console.WriteLine($"{kvp.Key} : {kvp.Value}");
            }

            // récupérer la liste des clés
            List<string> k = mois.Keys.ToList();

            // récupérer la liste des values
            List<int> v = mois.Values.ToList();

            Console.ReadKey();
        }
    }
}
