﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_07
{
    class Program
    {
        static Random generateor = new Random();
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                // Ne pas toucher l'itérateur
                //i--;
                Console.WriteLine("X");
            }

            List<string> liste = new List<string>() { "riri", "fifi", "loulou" };
            foreach(string item in liste)
            {
                //deconseillé
                liste.RemoveAt(2);

                Console.WriteLine(item);

                //deconseillé
                if (item == "fifi") break;
            }



            Random generator = new Random();
            for (int i = 0 ;i<10; i++)
            {

            int nombre =generator.Next(10,20);

            Console.WriteLine(nombre);
            }

            Console.ReadKey();
        }
    }
}
