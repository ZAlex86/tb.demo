﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            /*opérateurs sur les nombres*/
            int nb1 = 42;
            int nb2 = 43;
            Console.WriteLine(42 + 43);
            Console.WriteLine(42 - 43);
            Console.WriteLine(42 * 43);
            Console.WriteLine(42 / 43);
            Console.WriteLine(45 % 43);

            Console.WriteLine(nb1++);
            Console.WriteLine(nb1);
            Console.WriteLine(++nb2);

            Console.WriteLine(nb1 -= 2);

            Console.WriteLine(nb1 *= 2);

            // Concaténation
            Console.WriteLine("Hello " + "World");
            Console.WriteLine("Le nombre 1 = " + nb1 + " et le nombre 2 = " + nb2);
            Console.WriteLine($"Le nombre 1 = { nb1 } et le nombre 2 = { nb2 }");

            string tableau = "nom\tprenom\nLy\tKhun";

            Console.WriteLine(tableau);

            //string path = "c:\\users\\desktop";
            string path = @"c:\users\desktop";
            Console.WriteLine(path);

            string maChaine = "Hello ";
            maChaine += "world !!!";
            Console.WriteLine(maChaine);

            Console.ReadKey();
        }
    }
}
