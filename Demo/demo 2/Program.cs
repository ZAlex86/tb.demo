﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("veuillez entrer 2 nombres");
            string reponse1 = Console.ReadLine();
            string reponse2 = Console.ReadLine();

            //convertion string -> entier

            int nombre1 = int.Parse(reponse1);
            int nombre2 = int.Parse(reponse2);

            //conversion entier -> string

            string valeur = nombre1.ToString();

            Console.WriteLine(reponse1 + reponse2);
            Console.ReadKey();
        }
    }
}
