﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exobonus
{
    class Program
    {
        static void Main(string[] args)
        {
            //1   Variables / Conversion:
            //1.1 Creer une variable a et une variable b et inverser les valeurs de la variable a avec celle de b.
            //1.2 Demander à l'utilisateur d'entrer un nombre entre 1 et 10 et afficher le carré de ce nombre


            #region exob1.1
            //1.1
            //int a = 10;
            //int b = 5;

            //Console.WriteLine("la valeur de a est de " + a);
            //Console.WriteLine("la valeur de b est de " + b);
            //a = a + b;
            //b = a - b;
            //a = a - b;

            //Console.WriteLine("la valeur de a est de " + a);
            //Console.WriteLine("la valeur de b est de " + b);
            //Console.ReadKey();
            #endregion




            #region exob1.2
            //1.2

            //Console.WriteLine("veuillez entrer un nombre entre 1 et 10 ");

            //bool p = true;
            //while (p == true)
            //{
            //    string entree = Console.ReadLine();
            //    int nbr = int.Parse(entree);

            //    if (nbr <= 10 && nbr >= 1)
            //    {
            //        int carre = nbr * nbr;
            //        Console.WriteLine("le carré de " + nbr + " est de " + carre);
            //        p = false;

            //    }
            //    else
            //    {
            //        Console.WriteLine("le nombre doit être compris entre 1 et 10");
            //        p = true;
            //    }
            //}

            //Console.ReadKey(); 
            #endregion

            //2   Structures conditionnelles:
            //    2.1 Demander à l'utilisateur d'entrer 2 nombres et un opérateur(+,-,*,/);
            //    En fonction de l'opérateur afficher le résultat attention (division par 0).

            //    2.2 Récupérer la l'heure du jour (DateTime.Now.Hour) s'il est entre 9 et 17(exclus) heures
            //    afficher "Au boulot !!" sinon afficher "zzzz"
            //    Améliorer le programme pour afficher "Bon appétit s'il est entre 12 et 13(exclus) heures".


            #region exob2.1
            //bool operateur = true;         

            //Console.WriteLine("veuillez entrer le 1 nombre");
            //int.TryParse(Console.ReadLine(), out int nb1);

            //while (operateur==true)
            //{
            //    Console.WriteLine("veuillez enter un operateur");
            //    string signe = Console.ReadLine();

            //    if (signe == "+" || signe == "-" || signe == "*" || signe == "/")
            //    {
            //        operateur = false;
            //    }
            //    else
            //    {
            //        Console.WriteLine("operateur incorrect");
            //        operateur = true;

            //    }

            //    Console.WriteLine("veuillez entrer le 2 nombre");
            //    int.TryParse(Console.ReadLine(), out int nb2);

            //    if (signe =="+")
            //    {
            //        int resultat = nb1 + nb2;
            //        Console.WriteLine("le resultat de " + nb1 + signe + nb2 + " = " + resultat);
            //        Console.ReadKey();
            //    }
            //    else if (signe == "-")
            //    {
            //        int resultat = nb1 - nb2;
            //        Console.WriteLine("le resultat de " + nb1 + signe + nb2 + " = " + resultat);
            //        Console.ReadKey();
            //    }
            //    else if (signe == "*")
            //    {
            //        int resultat = nb1 * nb2;
            //        Console.WriteLine("le resultat de " + nb1 + signe + nb2 + " = " + resultat);
            //        Console.ReadKey();
            //    }
            //    else if (signe == "/")
            //    {
            //        if (nb2 == 0)
            //        {
            //            Console.WriteLine("operation impossible");
            //        }
            //        else
            //        {
            //            int resultat = nb1 / nb2;
            //            Console.WriteLine("le resultat de " + nb1 + signe + nb2 + " = " + resultat);
            //            Console.ReadKey();
            //        }


            //    Console.ReadKey();
            //    }

            //} 
            #endregion

            #region exob2.2
            //int time = DateTime.Now.Hour;

            //if (time >= 9 && time < 17 )
            //{
            //    Console.WriteLine("Au boulot !!");
            //    if(time =>12 && time <13) //bizarre
            //    {
            //        Console.WriteLine("Bon appétit");
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("zzzz");
            //}
            //Console.ReadKey(); 
            #endregion


            //3   Boucles:
            //3.1 Creer un tableau Contenant des chaines de caractères et le remplir avec qques mots
            //Afficher à l'aide de la boucle "for" le contenu du tableau
            //Afficher à l'aide d'une boucle "foreach" le contenu du tableau


            //3.2 Demander à l'utilisateur de d'entrer un nombre entre 1 et 20
            //Afficher la table de multiplication ce nombre jusque 20.
            //ex: 4 X 1 = 4, 4 X 2 = 8, 4 X 3 = 12, ... , 4 X 19 = 76, 4 X 20 = 80

            #region exob3.1
            //string[] tableau = new string[] { "pc", "gsm","a","b","ordinateur" };

            //for (int i = 0; i < tableau.Length ; i++)
            //{
            //    Console.WriteLine(tableau[i]);
            //}
            //Console.ReadKey();

            //foreach (string item in tableau)
            //{
            //    Console.WriteLine(item);
            //}
            //Console.ReadKey(); 
            #endregion


            #region exob3.2
            //Console.WriteLine("veuilliez entrer un nombre entre 1 et 20");
            //int.TryParse(Console.ReadLine(), out int nb);

            //for (int i = 1; i <= 20; i++)
            //{
            //    Console.WriteLine(""+nb+"*"+i+"="+""+nb*i);


            //}
            //Console.ReadKey(); 
            #endregion


            //3.3 A l'aide de 2 boucles créer un programme pour afficher les tables de multiplication de 1 à 10

            //3.4 A l'aide d'une boucle "while" demander à l'utilisateur d'entrer
            //autant de nombres qu'il le souhaite (la boucle s'arrête lorsqu'il n'entre pas un nombre correcte)
            //Afficher dans la console la somme de tous ces nombres.

            //3.5 Demander à l'utilisateur de rentrer 5 nombres différents.
            //Afficher ensuite la liste triée dans la console.

            #region exob3.3
            //for (int i = 1; i <= 10; i++)
            //{
            //    for (int j = 1; j <= 10; j++)
            //    {
            //        Console.WriteLine(""+i+"*"+j+"="+i*j);
            //    }
            //}
            //Console.ReadKey(); 
            #endregion


        }
    }
}
