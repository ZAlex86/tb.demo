﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_5
{
    class Program
    {
        static void Main(string[] args)
        {
            // opérations sur les type
            var maVariable = 1;
            Console.WriteLine(maVariable.GetType());
            Console.WriteLine(typeof(DateTime));
            if (maVariable is double)
            {
                Console.WriteLine(maVariable * 3);
            }
            else
            {
                Console.WriteLine("opération impossible");
            }
            Console.ReadKey();
        }
    }
}
