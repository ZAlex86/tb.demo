﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perso
{
    class Program
    {
        static void Main(string[] args)
        {

            //initialisation d'un tableau et remplissage des éspaces.
            int[,] tabgrille = new int[7, 7];
            int joueur1 = 1;
            int joueur2 = 2;
            int tourjoueur;
            int compteur = 42;

            
            

            //interface graphique
            InterfaceGraph(tabgrille);
            bool ganger = false;
            int vainqueur = 0;
            while (!ganger)
            {
                if (compteur % 2 == 0)
                {
                    tourjoueur = joueur1;

                }
                else
                {
                    tourjoueur = joueur2;
                }                
                Console.WriteLine("c\'est le tour du joueur"+ tourjoueur);
                Console.WriteLine("veuillez enter la colonne entre 0 et 6");
                int.TryParse(Console.ReadLine(), out int colonne);
                if (tabgrille[0, colonne] == 0)
                {
                    tabgrille[0, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 0))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if(checkhori(tabgrille, colonne, 0))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    compteur -= 1;
                }
                else if (tabgrille[1, colonne] == 0)
                {
                    tabgrille[1, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 1))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 1))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }

                    compteur -= 1;
                }
                else if (tabgrille[2, colonne] == 0)
                {
                    tabgrille[2, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 2))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 2))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }

                    compteur -= 1;
                }
                else if (tabgrille[3, colonne] == 0)
                {
                    tabgrille[3, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 3))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 3))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }

                    compteur -= 1;
                }
                else if (tabgrille[4, colonne] == 0)
                {
                    tabgrille[4, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 4))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 4))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }


                    compteur -= 1;
                }
                else if (tabgrille[5, colonne] == 0)
                {
                    tabgrille[5, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 5))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 5))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }

                    compteur -= 1;
                }
                else if (tabgrille[6, colonne] == 0)
                {
                    tabgrille[6, colonne] = tourjoueur;
                    if (checkverti(tabgrille, colonne, 6))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }
                    else if (checkhori(tabgrille, colonne, 6))
                    {
                        ganger = true;
                        vainqueur = tourjoueur;
                    }

                    compteur -= 1;
                }
                else
                {
                    Console.WriteLine("cette colonne est  déjà remplie");
                    Console.ReadKey();
                }


                Console.Clear();
                InterfaceGraph(tabgrille);

            }
            Console.WriteLine("le joueur "+vainqueur+" à gagner!!!");
            Console.ReadKey();



        }
        public static bool checkverti(int[,] tabgrille,int colonne,int hauteur)
        {
            if (hauteur < 3)
                return false;
            else if (tabgrille[hauteur, colonne] != tabgrille[hauteur - 1, colonne] || tabgrille[hauteur, colonne] != tabgrille[hauteur - 2, colonne] || tabgrille[hauteur, colonne] != tabgrille[hauteur - 3, colonne])
                return false;
            else
                return true;
        }

        public static bool checkhori(int[,] tabgrille,int colonne,int hauteur)
        {
            if ((colonne == 0) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 3]))
                return true;
            else if (((colonne == 1) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2])) || ((colonne == 1) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 3])))
                return true;

            else if ((((colonne == 2) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1])) || ((colonne == 2) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2])) || ((colonne == 2) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 3]))))
                return true;

            else if (((colonne == 3) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 3]))|| ((colonne == 3) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2]&& tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1]&& tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1]))|| ((colonne == 3) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1]&& tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1]&& tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2]))||((colonne==3)&& tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1]&& tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2]&& tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 3]))
                return true;
            else if (((colonne == 4) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 2])) || ((colonne == 4) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1])) || ((colonne == 4) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 3]))) 

                return true;
            else if (((colonne == 5) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 3])) || ((colonne == 5) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne + 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2])))
                return true;
            else if ((colonne == 6) && (tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 1] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 2] && tabgrille[hauteur, colonne] == tabgrille[hauteur, colonne - 3]))
                return true;
            else
                return false;
        }

        

        

        public static void InterfaceGraph(int[,] tabgrille)
        {
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[6, 0] + " | " + tabgrille[6, 1] + " | " + tabgrille[6, 2] + " | " + tabgrille[6, 3] + " | " + tabgrille[6, 4] + " | " + tabgrille[6, 5] + " | " + tabgrille[6, 6] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[5, 0] + " | " + tabgrille[5, 1] + " | " + tabgrille[5, 2] + " | " + tabgrille[5, 3] + " | " + tabgrille[5, 4] + " | " + tabgrille[5, 5] + " | " + tabgrille[5, 6] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[4, 0] + " | " + tabgrille[4, 1] + " | " + tabgrille[4, 2] + " | " + tabgrille[4, 3] + " | " + tabgrille[4, 4] + " | " + tabgrille[4, 5] + " | " + tabgrille[4, 6] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[3, 0] + " | " + tabgrille[3, 1] + " | " + tabgrille[3, 2] + " | " + tabgrille[3, 3] + " | " + tabgrille[3, 4] + " | " + tabgrille[3, 5] + " | " + tabgrille[3, 6] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[2, 0] + " | " + tabgrille[2, 1] + " | " + tabgrille[2, 2] + " | " + tabgrille[2, 3] + " | " + tabgrille[2, 4] + " | " + tabgrille[2, 5] + " | " + tabgrille[2, 6] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[1, 0] + " | " + tabgrille[1, 1] + " | " + tabgrille[1, 2] + " | " + tabgrille[1, 3] + " | " + tabgrille[1, 4] + " | " + tabgrille[1, 5] + " | " + tabgrille[1, 6] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | " + tabgrille[0, 0] + " | " + tabgrille[0, 1] + " | " + tabgrille[0, 2] + " | " + tabgrille[0, 3] + " | " + tabgrille[0, 4] + " | " + tabgrille[0, 5] + " | " + tabgrille[0, 6] + " | ");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" _____________________________");
            Console.WriteLine(" | 0 | 1 | 2 | 3 | 4 | 5 | 6 |");
        }
    }
}
